//
//  AppDelegate.h
//  ForGitMessup
//
//  Created by Peter Molnar on 18/09/2015.
//  Copyright © 2015 Peter Molnar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

