//
//  main.m
//  ForGitMessup
//
//  Created by Peter Molnar on 18/09/2015.
//  Copyright © 2015 Peter Molnar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
